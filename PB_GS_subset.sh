##################
### SAVE VARIABLES
##################

outdir=$1 # Output directory
subset=$2 # Percentage of reads to subset
seed=$3 # Seed to use
genome=$4 # Reference genome to use - Full path
tChr=$5 # On-target chromosome
tSt=$6 # On-target start position
tEnd=$7 # On-target end position
indir=$8 # Directory where sample of 100% (any) subset was run
prefix=$9

prefix=`echo $prefix | rev | cut -d'/' -f 1 | rev`
R1=(${indir}/filtered/$prefix*_R1*.fasta*)
R2=(${indir}/filtered/$prefix*_R2*.fasta*)
sample=(`ls ${R1[@]} | rev | cut -d'/' -f 1 | rev | cut -d'_' -f 1`)

##################
### PIPELINE
##################

### Create output directory

if [ ! -d "$outdir" ]; then
        mkdir $outdir
fi
if [ ! -d "$outdir/mapped" ]; then
        mkdir $outdir/mapped
fi
if [ ! -d "$outdir/peaks" ]; then
        mkdir $outdir/peaks
fi
if [ ! -d "$outdir/subset" ]; then
        mkdir $outdir/subset
fi
if [ ! -d "$outdir/subset/temp" ]; then
        mkdir $outdir/subset/temp
fi

### Subset raw imput reads depending on percentage
# Use a different seed for each replicate

for i in "${!R1[@]}"; do
	echo "Subsetting read files - Sample ${sample[$i]}"
	# Retrieve on and off-target read names
	echo "$tChr $tSt $tEnd" > $outdir/subset/temp/region.bed
	samtools view -b $indir/mapped/${sample[$i]}_filtered_mapped.sorted.bam -L $outdir/subset/temp/region.bed -U $outdir/subset/temp/off-target.bam > $outdir/subset/temp/on-target.bam
	samtools view $outdir/subset/temp/on-target.bam | cut -f1 | sort | uniq > $outdir/subset/temp/on_target_names.txt
	samtools view $outdir/subset/temp/off-target.bam | cut -f1 | sort | uniq > $outdir/subset/temp/off_target_names.txt
	# Calculate on-target reads to retrieve
	READS=`wc -l $outdir/subset/temp/on_target_names.txt | awk '{print $1}'`
	SUB=`echo "($READS*$subset)/100" | bc`
	# Output off-target reads
	seqtk subseq ${R1[$i]} $outdir/subset/temp/off_target_names.txt > $outdir/subset/${sample[$i]}_R1_subset.fastq
	seqtk subseq ${R2[$i]} $outdir/subset/temp/off_target_names.txt > $outdir/subset/${sample[$i]}_R2_subset.fastq
	# Output on-target reads
	seqtk subseq ${R1[$i]} $outdir/subset/temp/on_target_names.txt > $outdir/subset/temp/on-target_R1_subset.fastq
	seqtk subseq ${R2[$i]} $outdir/subset/temp/on_target_names.txt > $outdir/subset/temp/on-target_R2_subset.fastq
	# Subset on-target reads and add them to off-target reads
	seqtk sample -s $seed $outdir/subset/temp/on-target_R1_subset.fastq $SUB >> $outdir/subset/${sample[$i]}_R1_subset.fastq
	seqtk sample -s $seed $outdir/subset/temp/on-target_R2_subset.fastq $SUB >> $outdir/subset/${sample[$i]}_R2_subset.fastq
done

### Map selected reads against genome

for i in "${!R1[@]}"; do
	echo "Read Genome mapping - Sample ${sample[$i]}"
	bwa mem $genome $outdir/subset/${sample[$i]}_R1_subset.fastq \
		$outdir/subset/${sample[$i]}_R2_subset.fastq \
		-o $outdir/mapped/${sample[$i]}_mapped.sam
	samtools sort -O BAM -T tmp -o $outdir/mapped/${sample[$i]}_mapped.sorted.bam \
		$outdir/mapped/${sample[$i]}_mapped.sam
	samtools index $outdir/mapped/${sample[$i]}_mapped.sorted.bam \
		$outdir/mapped/${sample[$i]}_mapped.sorted.bam.bai
done

### Peak detection

for i in "${!R1[@]}"; do
	echo "Peak detection - Sample ${sample[$i]}"
	sudo docker run -v $PWD:$PWD fooliu/macs2 callpeak \
		-t $PWD/$outdir/mapped/${sample[$i]}_mapped.sorted.bam \
		-n calls -f BAM -g hs -s 250 --nomodel \
		--outdir $PWD/$outdir/peaks/${sample[$i]}/
done