# README #

The analysis workflow is presented here in support of Pallarès, et al. manuscript "Find and cut-and-transfer mammalian genome engineering (FiCAT)".

The analysis workflow used in this manuscript has been made available as shell scripts.

* runPermutations.sh runs the necessary scripts for the analysis of FiCAT isnertions and the calulation of LOD (Limit of Detection).

  + The main shell script will run PB_GS_subset.sh to compute permutations in order to calculate LOD.

  + The main shell script will run PB_GS_NOsubset.sh to tun the code used for the analysis of NGS on-target insertions. 

Custom scripts used in the workflow are shared inside Scripts folder.
Reference sequences are shared inside References folder.
Scripts from "guideseq: The GUIDE-Seq Analysis Package" (https://github.com/aryeelab/guideseq) are used and shared inside GuideSeq_aryeelab forder for convenience.

The raw data files used in this analysis can be accessed via European Nucleotide Archive (ENA) under the Study accession number PRJEB39575.

