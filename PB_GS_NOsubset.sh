##################
### SAVE VARIABLES
##################

outdir=$1 # Output directory
prefix=$2 # Input directory with raw fastq files followed by prefix of files to select
plasmid=$3 # Reference plasmid to use - Full path
genome=$4 # Reference genome to use - Full path
pname=$5 # Plasmid sequence name
tChr=$6 # On-target chromosome
tSt=$7 # On-target start position
tEnd=$8 # On-target end position

raw=${prefix}*.fastq*
R1=(${prefix}*_R1_*.fastq*)
R2=(${prefix}*_R2_*.fastq*)
I1=(${prefix}*_I1_*.fastq*)
I2=(${prefix}*_I2_*.fastq*)
sample=(`ls ${R1[@]} | rev | cut -d'/' -f 1 | rev | cut -d'_' -f 1`)

##################
### PIPELINE
##################

### Create output directory

if [ ! -d "$outdir" ]; then
        mkdir $outdir
fi
if [ ! -d "$outdir/clusters" ]; then
        mkdir $outdir/clusters
fi
if [ ! -d "$outdir/filtered" ]; then
        mkdir $outdir/filtered
fi
if [ ! -d "$outdir/mapped" ]; then
        mkdir $outdir/mapped
fi
if [ ! -d "$outdir/peaks" ]; then
        mkdir $outdir/peaks
fi
if [ ! -d "$outdir/umitagged" ]; then
        mkdir $outdir/umitagged
fi


### Tag reads with umi

for i in "${!R1[@]}"; do
	echo "UMI tagging - Sample ${sample[$i]}"
	python GuideSeq_aryeelab/guideseq.py umitag --read1 ${R1[$i]} \
		--read2 ${R2[$i]} \
		--index1 ${I1[$i]} \
		--index2 ${I2[$i]} \
		--outfolder $outdir/
done

### Create consensus sequence from umi clusters

for i in "${!R1[@]}"; do
	echo "Consensus sequence - Sample ${sample[$i]}"
	python Scripts/getUMI.py -i $outdir/umitagged/${sample[$i]}_*.r1.umitagged.fastq \
		-o $outdir/clusters/${sample[$i]}_umis.fasta
	usearch -fastx_uniques $outdir/clusters/${sample[$i]}_umis.fasta \
		-fastaout $outdir/clusters/${sample[$i]}_umis_uniq.fasta \
		-sizeout -minuniquesize 1 -relabel umi -strand plus
	usearch -cluster_fast $outdir/clusters/${sample[$i]}_umis_uniq.fasta \
		-id 0.9 -centroids $outdir/clusters/${sample[$i]}_centroids.fasta \
		-sizein -sizeout -strand plus -minsize 2
	usearch -usearch_global $outdir/clusters/${sample[$i]}_umis.fasta \
		-db $outdir/clusters/${sample[$i]}_centroids.fasta \
		-id 0.9 -blast6out $outdir/clusters/${sample[$i]}_alignment.b6 -strand plus
	python Scripts/clustering.py -a $outdir/clusters/${sample[$i]}_alignment.b6 \
		-o $outdir/clusters/${sample[$i]}_consensus_R1.fasta \
		-i $outdir/umitagged/${sample[$i]}_*.r1.umitagged.fastq
	python Scripts/clustering.py -a $outdir/clusters/${sample[$i]}_alignment.b6 \
		-o $outdir/clusters/${sample[$i]}_consensus_R2.fasta \
		-i $outdir/umitagged/${sample[$i]}_*.r2.umitagged.fastq
done

### Mapping against plasmid

for i in "${!R1[@]}"; do
	echo "Plasmid mapping - Sample ${sample[$i]}"
	bwa mem $plasmid $outdir/clusters/${sample[$i]}_consensus_R1.fasta \
		$outdir/clusters/${sample[$i]}_consensus_R2.fasta \
		-o $outdir/mapped/${sample[$i]}_consensus_plasmid-mapped.sam
	samtools sort -O BAM -T tmp -o $outdir/mapped/${sample[$i]}_consensus_plasmid-mapped.sorted.bam \
		$outdir/mapped/${sample[$i]}_consensus_plasmid-mapped.sam
	samtools index $outdir/mapped/${sample[$i]}_consensus_plasmid-mapped.sorted.bam \
		$outdir/mapped/${sample[$i]}_consensus_plasmid-mapped.sorted.bam.bai
done

### Selecting reads mapping against plasmid
### Map selected reads against genome

for i in "${!R1[@]}"; do
	echo "Read filtering and Genome mapping - Sample ${sample[$i]}"
	awk -v n="$pname" '$3 == n {print $1}' $outdir/mapped/${sample[$i]}_consensus_plasmid-mapped.sam | \
		sort | uniq -c | awk '{print $2}' > $outdir/mapped/${sample[$i]}_selected.txt
	seqtk subseq $outdir/clusters/${sample[$i]}_consensus_R1.fasta \
		$outdir/mapped/${sample[$i]}_selected.txt > $outdir/filtered/${sample[$i]}_filtered_R1.fasta
	seqtk subseq $outdir/clusters/${sample[$i]}_consensus_R2.fasta \
		$outdir/mapped/${sample[$i]}_selected.txt > $outdir/filtered/${sample[$i]}_filtered_R2.fasta
	bwa mem $genome $outdir/filtered/${sample[$i]}_filtered_R1.fasta \
		$outdir/filtered/${sample[$i]}_filtered_R2.fasta \
		-o $outdir/mapped/${sample[$i]}_filtered_mapped.sam
	samtools sort -O BAM -T tmp -o $outdir/mapped/${sample[$i]}_filtered_mapped.sorted.bam \
		$outdir/mapped/${sample[$i]}_filtered_mapped.sam
	samtools index $outdir/mapped/${sample[$i]}_filtered_mapped.sorted.bam \
		$outdir/mapped/${sample[$i]}_filtered_mapped.sorted.bam.bai
done

### Peak detection

for i in "${!R1[@]}"; do
	echo "Peak detection - Sample ${sample[$i]}"
	sudo docker run -v $PWD:$PWD fooliu/macs2 callpeak \
		-t $PWD/$outdir/mapped/${sample[$i]}_filtered_mapped.sorted.bam \
		-n calls -f BAM -g hs -s 250 --nomodel \
		--outdir $PWD/$outdir/peaks/${sample[$i]}/
done

### Filter only reads in significant insertions

for i in "${!R1[@]}"; do
	echo "Select significant insertions - Sample ${sample[$i]}"
	if [ -s $outdir/peaks/${sample[$i]}/calls_summits.bed ]
	then
		samtools view -b -h -L $outdir/peaks/${sample[$i]}/calls_summits.bed \
			$outdir/mapped/${sample[$i]}_filtered_mapped.sorted.bam > \
			$outdir/mapped/${sample[$i]}_significant.sorted.bam
		samtools index $outdir/mapped/${sample[$i]}_significant.sorted.bam \
			$outdir/mapped/${sample[$i]}_significant.sorted.bam.bai	
	else
		echo "Sample ${sample[$i]} has no significant peaks"
	fi
done

### Plot Karyoplote

for i in "${!R1[@]}"; do
	echo "Karyplote - Sample ${sample[$i]}"
	Rscript Scripts/plotCoverage.R $genome \
		$outdir/peaks/${sample[$i]} \
		$outdir/mapped/${sample[$i]}_significant.sorted.bam \
		$tChr $tSt $tEnd
done
