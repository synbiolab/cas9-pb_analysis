def write_consensus(inN, outN, readsN):
	"""
	Write consensus sequence using all reads for each UMI
	Using a dumb consensus with a thershold of 0.5
	"""
	align = open(inN)
	clusters = {}
	for line in align:
	    line = line.strip()
	    line = line.split("\t")
	    read = line[0].split(" ")[0]
	    umi = line[1]
	    if not umi in clusters.keys():
	       clusters[umi]={}
	       clusters[umi]["names"]=[read]
	       clusters[umi]["seqs"]=[]
	    else:
	        clusters[umi]["names"].append(read)
	reads = SeqIO.to_dict(SeqIO.parse(readsN, 'fastq'))
	for u in clusters.keys():
	    for n in clusters[u]["names"]:
	        clusters[u]["seqs"].append(str(reads[n].seq))
	out=open(outN, 'w')
	for u in clusters.keys():
	    out.write(">{}\n".format(u))
	    sr = [SeqRecord(Seq(s, generic_dna), id=s) for s in clusters[u]["seqs"]]
	    if len(sr) > 1:
		    temp = open("temp.fasta", 'w')
		    for s in sr:
		        temp.write(s.format("fasta"))
		    temp.close()
		    align = MafftCommandline(input="temp.fasta", adjustdirection=True)
		    stdout, stderr = align()
		    a = Bio.AlignIO.read(StringIO(stdout), "fasta")
		    summary_align = AlignInfo.SummaryInfo(a)
		    consensus = summary_align.dumb_consensus(threshold=0.5, ambiguous='N')
	    else:
		    consensus = str(sr[0].seq)
	    out.write("{}\n".format(str(consensus)))
	out.close()



if __name__ == "__main__":
	from Bio import SeqIO
	from Bio.Alphabet import generic_dna
	from Bio.Seq import Seq
	from Bio.SeqRecord import SeqRecord
	from Bio.Align import MultipleSeqAlignment
	from Bio.Align import AlignInfo
	from Bio.Align.Applications import MafftCommandline
	import Bio.AlignIO
	from io import StringIO
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("-i", help="Input fastq file name with sequencing reads", action="store")
	parser.add_argument("-o", help="Output fasta file name for consensus reads", action="store")
	parser.add_argument("-a", help="Alignment file name in blast6 format", action="store")
	args = parser.parse_args()

	write_consensus(args.a, args.o, args.i)

