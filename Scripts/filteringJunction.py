def filterReads(inBam, out5, out3, pos5, pos3, chrName):
	"""
	Filter reads covering 5' and 3' junctions and output read names
	"""
	bamFile = pysam.AlignmentFile(inBam, 'rb')
	bamReads = bamFile.fetch(chrName, until_eof=True)
	sel5 = open(out5, 'w')
	sel3 = open(out3, 'w')
	NO = True
	for r in bamReads:
	    if not r.is_unmapped:
	        if int(r.reference_start) < int(pos5)-5 and int(r.reference_end) > int(pos5)+5:
	            sel5.write("{}\n".format(r.qname))
		    NO = False
	        if int(r.reference_start) < int(pos3)-5 and int(r.reference_end) > int(pos3)+5:
	            sel3.write("{}\n".format(r.qname))
		    NO = False
		if NO:
			

if __name__ == "__main__":
	import pysam
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("-i", help="Input bam file", action="store")
	parser.add_argument("-o5", help="Output text file name for 5' junction filtered read names", action="store")
	parser.add_argument("-o3", help="Output text file name for 3' junction filtered read names", action="store")
	parser.add_argument("-p5", help="5' junction position", action="store")
	parser.add_argument("-p3", help="3' junction position", action="store")
	parser.add_argument("-n", help="Reference equence name", action="store")
	args = parser.parse_args()

	filterReads(args.i, args.o5, args.o3, args.p5, args.p3, args.n)

