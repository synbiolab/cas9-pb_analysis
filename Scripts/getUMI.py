def getUMIs(file):
	"""
	Select UMI sequence: UMI + 6bp read 1 + 6bp read 2 
	"""
	data = SeqIO.parse(file, 'fastq')
	umis = [d.description.split(" ")[-1] for d in data]
	umis = ["".join(u.split("_")) for u in umis]
	return umis

def getNames(file):
	""" 
	Retrieve read names
	"""
	data = SeqIO.parse(file, 'fastq')
	names = [d.name for d in data]
	return names

def printOutput(file, umis, names):
	"""
	Print read name and umi sequence in a fasta file
	"""
	out = open(file, 'w')
	for u, n in zip(umis, names):
		out.write(">{}\n".format(n))
		out.write("{}\n".format(u))
	out.close()

if __name__ == "__main__":
	from Bio import SeqIO
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("-i", help="Input fastq file with sequencing reads", action="store")
	parser.add_argument("-o", help="Output fasta file name for umi sequences", action="store")
	args = parser.parse_args()

	printOutput(args.o, getUMIs(args.i), getNames(args.i))
