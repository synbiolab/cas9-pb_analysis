def parsePeaks(inPeaks, OTchr, OTst, OTend):
	peaksFile = open(inPeaks)
	offTarget = {}
	onTarget = {"chr": OTchr, "start": OTst, "end": OTend}
	for line in itertools.islice(peaksFile, 25, None):
		line = line.strip()
		line = line.split("\t")
		chrom = line[0]
		start = int(line[1])
		end = int(line[2])
		name = line[9]
		if chrom == OTchr and start > OTst and end < OTend:
			onTarget = {"chr": chrom, "start": start, "end": end}
		else:
			offTarget[name] = {"chr": chrom, "start": start, "end": end}
	return (onTarget, offTarget, len(offTarget.keys())+1)

def paseBam(inBam, inPeaks, OTchr, OTst, OTend):
	bamFile = pysam.AlignmentFile(inBam, 'rb')
	TMR = bamFile.mapped # number of mapped reads
	
	OTreads = 0
	OfTreads = 0
	insertions = 0
	if os.path.isfile(inPeaks):
		onTarget, offTarget, insertions = parsePeaks(inPeaks, OTchr, OTst, OTend)
		reads = bamFile.fetch(onTarget["chr"], onTarget["start"], onTarget["end"], until_eof=True)
		for r in reads:
			OTreads += 1
		for off in offTarget.keys():
			reads = bamFile.fetch(offTarget[off]["chr"], offTarget[off]["start"], offTarget[off]["end"], until_eof=True)
			for r in reads:
				OfTreads += 1

	OTp = (OTreads/TMR)*100 # on target percentage from total mapped reads (noise)
	OTpi = ((OTreads+1)/(OfTreads+OTreads+1))*100 # on target percentage from insertions
	OfTp = (OfTreads/TMR)*100 # off target percentage from total mapped reads
	ratio = (OTreads+1)/(OfTreads+1) # ration on-target off-target
	insertions # total number of insertions (on-target + off-target)
	return (OTp, OTpi, OfTp, ratio, insertions)

def printResults(OTp, OTpi, OfTp, ratio, insertions, outN):
	outF = open(outN, 'w')
	outF.write("on-target percentage from total mapped reads (noise)\t{}%\n".format(OTp))
	outF.write("on-target percentage from insertions\t{}%\n".format(OTpi))
	outF.write("off-target percentage from total mapped reads\t{}%\n".format(OfTp))
	outF.write("ration on-target off-target\t{}\n".format(ratio))
	outF.write("total number of insertions (on-target + off-target)\t{}\n".format(insertions))
	outF.close()
			

if __name__ == "__main__":
	import pysam
	import argparse
	import itertools
	import os.path

	parser = argparse.ArgumentParser()
	parser.add_argument("-b", help="Input bam file", action="store")
	parser.add_argument("-p", help="Input peaks file (xls)", action="store")
	parser.add_argument("-o", help="Output file name", action="store")
	parser.add_argument("-c", help="on-target chromosome name", action="store")
	parser.add_argument("-s", help="on-target start position", action="store")
	parser.add_argument("-e", help="on-target end position", action="store")
	args = parser.parse_args()

	OTp, OTpi, OfTp, ratio, insertions = paseBam(args.b, args.p, args.c, int(args.s), int(args.e))
	printResults(OTp, OTpi, OfTp, ratio, insertions, args.o)
