def printFC(file, chrName, targSt, targEnd):
	peaks = open(file)
	FCs = []
	zero = True
	for line in peaks:
		line = line.strip()
		line = line.split('\t')
		if not line[0].startswith("#") and len(line)>1:
			if line[0] == chrName and int(line[1]) < targEnd and int(line[2]) > targSt:
				FCs.append(float(line[7]))
				zero = False
				break
	if zero:
		FCs.append(0)
	return FCs

def retrieveFiles(rootdir, subsets, sample):
	files = {}
	for n in subsets:
		files[n] = glob.glob(rootdir + n + "_*/peaks/" + sample + "/calls_peaks.xls")
	return files

def retrieveFC(files, subsets, permutations, chrName, targSt, targEnd):
	FCs = {}
	for n in subsets:
		FCs[n] = []
		for file in files[n]:
			FCs[n].extend(printFC(file, chrName, targSt, targEnd))
		FCs[n].extend([0]*(permutations - len(files[n])))
	return FCs

def printOutput(out, FCs, subsets):
	output = open(out, 'w')
	for n in subsets:
		for fc in FCs[n]:
			output.write("{}\t{}\n".format(n, fc))
	output.close()

if __name__ == "__main__":
	from Bio import SeqIO
	import argparse
	import glob

	parser = argparse.ArgumentParser()
	parser.add_argument("-r", "--rootdir", help="Rood directory containing subset output directories", action="store", required=True)
	parser.add_argument("--subsets", help="List of percentages used for subset", nargs='+', required=True)
	parser.add_argument("-s", "--sample", help="Sample name (folder)", action="store", required=True)
	parser.add_argument("-p", "--permutations", help="Number of permutations done", action="store", required=True)
	parser.add_argument("-c", "--chrName", help="On-target chromosome name", action="store", required=True)
	parser.add_argument("-st", "--targSt", help="On-target start position", action="store", required=True)
	parser.add_argument("-en", "--targEnd", help="On-target end position", action="store", required=True)
	parser.add_argument("-o", "--output", help="Output file name for fold change tsv", action="store", required=True)
	args = parser.parse_args()

	FCs = retrieveFC(retrieveFiles(args.rootdir, args.subsets, args.sample), args.subsets, int(args.permutations), args.chrName, int(args.targSt), int(args.targEnd))
	printOutput(args.output, FCs, args.subsets)